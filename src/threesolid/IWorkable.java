package threesolid;
//To make sure this follows the open-close principle, the original iworker class was split into IFeedable and Iworkable

//Good example interface segregation principle, single responsibility principle and open close principle. 
//By having the ability to eat be its own separate interface, we can change the eat function 
//(i.e. the type of work done or time on the clock) without having to change any extra code, while also
//letting us keep code clean and easy to understand without needing to rewrite it for every person working.

interface IWorkable {
	public void work();
}