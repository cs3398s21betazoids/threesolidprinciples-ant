package threesolid;

// This Class is a good example of a of the interface segregation
// principle bcause it allows Iworker to connect Ifeedable and
// Iworkable.
// This class also follows the open close Principle because this
// has one function and that is to extend IFeedable and Iworkable
interface IWorker extends IFeedable, IWorkable {
}
