package threesolid;

// This Class exemplfies the Single Responsiblitiy principle being that
// his class has one responsibility and and that is to have the "Robot"
// to do "work" mindlessly.
// This also Follows the Open-Close Principle that allows Robot to be
// accessed from IWorkable but not modified by it.
class Robot implements IWorkable {
	public void work() {
		// ....working mindlessly
	}
}
