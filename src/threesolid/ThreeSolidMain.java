package threesolid;

import java.awt.*;        // Using AWT container and component classes
import java.awt.event.*;  // Using AWT event classes and listener interfaces
import java.io.*;

// Adding a comment.


public class ThreeSolidMain
{   

   public static Manager tsManager = new Manager();

   // The entry main() method
   public static void main(String[] args) 
   {
 
      try 
      {
         System.out.format("Starting ... \n");               
      } 
      catch (Exception main_except)
      {
         main_except.printStackTrace();
      }

      Robot manageRobot=new Robot();

      tsManager.setWorker(manageRobot);
      System.out.println("EVERYONE START WORKING!!!!!");
      tsManager.manage();

      

      SuperWorker bestWorker=new SuperWorker();

      Worker wageMonkey=new Worker();

      Robot freeLabor=new Robot();

      System.out.println("Robot:");
      freeLabor.work();

      System.out.println("Worker:");
      wageMonkey.work();
      wageMonkey.eat();

      System.out.println("SuperWorker:");
      bestWorker.work();
      bestWorker.eat();

      

      try 
      {
         System.out.format("Stopping ... \n");               
      } 
      catch (Exception main_except)
      {
         main_except.printStackTrace();
      }

      System.exit(0);

   }
}
