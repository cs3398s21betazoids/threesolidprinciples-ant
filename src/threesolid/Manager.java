package threesolid;


//We moved this to its own file to follow with the open close principle.
class Manager {
	IWorkable worker;

	public void Manager() {

	}
	public void setWorker(IWorkable w) {
		worker=w;
	}

	public void manage() {
		worker.work();
	}
}
