package threesolid;

//To make sure this follows the open-close principle, the original iworker class was split into IFeedable and Iworkable

//Good example interface segregation principle, single responsibility principle and open close principle. 
//By having the ability to eat be its own separate interface, we can change the eat function 
//(i.e. changing time allotted to eat or the cafeteria menu) without having to change any code, while also
//letting us keep code clean and easy to understand without needing to rewrite it for every eating worker. 
 

interface IFeedable { 
	public void eat();
}